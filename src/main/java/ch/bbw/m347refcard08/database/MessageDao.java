package ch.bbw.m347refcard08.database;

import ch.bbw.m347refcard08.RefCardProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Data Access Object for messages
 *
 * @author Peter Rutschmann
 * @version 01.05.2023
 */
@Component
@Slf4j
public class MessageDao {
   @Autowired
   RefCardProperties properties;

   public List<Message> findAll() {
      List<Message> result = new ArrayList<>();
      String sql = "Select * from message";
      try {
         Connection connection = DriverManager.getConnection(properties.getDbUrl(), properties.getDbUser(),
               properties.getDbPassword());
         PreparedStatement stmt = connection.prepareStatement(sql);
         ResultSet resultSet = stmt.executeQuery();
         while (resultSet.next()) {
            Message message = new Message();
            message.setId(Long.valueOf(resultSet.getString("id")));
            message.setContent(resultSet.getString("content"));
            message.setAuthor(resultSet.getString("author"));
            result.add(message);
         }
         result.forEach(x -> log.info("findAll():" + x));
         stmt.close();
         connection.close();
      } catch (SQLException e) {
         log.error("findAll(): SQL State: " + e.getSQLState() + " Message: " + e.getMessage());
      } catch (Exception e) {
         log.error(String.valueOf(Arrays.toString(e.getStackTrace())));
      }
      return result;
   }
}