package ch.bbw.m347refcard08;

import ch.bbw.m347refcard08.captcha.ReCaptchaValidationService;
import ch.bbw.m347refcard08.database.MessageDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * MainController class
 * @author Peter Rutschmann
 * @version 25.04.2023
 */
@Controller
@Slf4j
public class MainController {
   @Autowired
   RefCardData data;
   @Autowired
   RefCardProperties properties;
   @Autowired
   private ReCaptchaValidationService reCaptchaService;
   @Autowired
   private MessageDao dao;

   @GetMapping("/")
   public String getIndex(Model model){
      log.info("getIndex():" + properties);
      log.info("getIndex():" + reCaptchaService.getCaptchaSettings());
      model.addAttribute("environment", properties);
      model.addAttribute("recaptchaenviroment", reCaptchaService.getCaptchaSettings());
      model.addAttribute("data", data);
      model.addAttribute("messages", dao.findAll());
      return "index";
   }

   @PostMapping("/changeTheme")
   public String postIndex(@ModelAttribute("enviroment") RefCardProperties properties, Model model){
      log.info("postIndex(): " + properties);
      if (properties.getTheme() != null) this.properties.setTheme(properties.getTheme());
      log.info("postIndex(): " + this.properties);
      return "redirect:/";
   }

   @PostMapping("/validateCaptcha")
   public String postValidateCaptcha(@RequestParam(name="g-recaptcha-response") String captcha, Model model) {
      if (!reCaptchaService.validateCaptcha(captcha)) {
         log.warn("postValidateCaptcha: Captcha fails");
         log.warn("postValidateCaptcha: Captcha was not correct!");
         data.setCaptchaMessage("Captcha validation failed.");
         return "redirect:/";
      }
      log.info("postValidateCaptcha: Captcha is correct!");
      data.setCaptchaMessage("Captcha validation passed.");
      return "redirect:/";
   }
}
